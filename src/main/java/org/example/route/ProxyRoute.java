package org.example.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.example.process.Inspector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProxyRoute extends RouteBuilder {

  private static final Logger logger = LoggerFactory.getLogger(ProxyRoute.class);

  final
  private Inspector inspector;

  public ProxyRoute(Inspector inspector) {
    this.inspector = inspector;
  }

  @Override
  public void configure() throws Exception {

    from("jetty:https://0.0.0.0:9443?matchOnUriPrefix=true")
        .routeId("httpsProxyRoute")
        .setHeader(Exchange.HTTP_SCHEME, constant("https"))
        .to("direct:proxy");

    from("jetty:http://0.0.0.0:9090?matchOnUriPrefix=true")
        .routeId("httpProxyRoute")
        .setHeader(Exchange.HTTP_SCHEME, constant("http"))
        .to("direct:proxy");

    from("direct:proxy")
        .log(">>>>>>> ${date:now}")
        // .to("log:info?showHeaders=true")
        .log("CamelHttpScheme: ${headers.CamelHttpScheme}")
        .log("CamelHttpUrl:    ${headers.CamelHttpUrl}")
        .log("CamelHttpHost:   ${headers.Host}")
        .log("CamelHttpHost:   ${headers.CamelHttpHost}")
        .log("CamelHttpPort:   ${headers.CamelHttpPort}")
        .log("CamelHttpPath:   ${headers.CamelHttpPath}")
        .log("${headers.CamelHttpScheme}://${headers.Host}/${headers.CamelHttpPath}?bridgeEndpoint=true")
        .toD("${headers.CamelHttpScheme}://${headers.Host}/${headers.CamelHttpPath}?bridgeEndpoint=true")
        .log("response : \n${body}")
    ;
  }
}
